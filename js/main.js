// side navbar
const menu = document.querySelector(".menu");
menu.addEventListener("click", () => {
  document.querySelector(".sm-s-nav").classList.add("reveal-sm-nav");
});

const closeMenu = document.querySelector(".close-menu");
closeMenu.addEventListener("click", () => {
  document.querySelector(".sm-s-nav").classList.remove("reveal-sm-nav");
});


 /**  jquery  */
$(document).ready(function() {

  // internal link smooth scrolling + disable sidebar
  // listen to click events on all elements with class "s-scroll"
  $(".s-scroll").click(function(e) {
    
    // get the href attribute of the clciked element
    let anchor = $(this).attr("href");
    const sideBar = $(".sm-s-nav");

    scroll(anchor);

    //collapse sidebar if active
    if (sideBar.hasClass("reveal-sm-nav")) {
      sideBar.removeClass("reveal-sm-nav");
    }

    // active nav item indicator
    // $(".nav-item").removeClass("active");
    // $(this).addClass("active");
  });

  // check for hash part of url after page load
  // if it exist, scroll to the position of the hash
  if (window.location.hash) {
    let hash = window.location.hash;
    scroll(hash);
  }

  // scroll to internal link position function
  function scroll(anchor) {
    $("html, body").animate({ scrollTop: $(anchor).offset().top - 150}, 1000);
  }


});

// window.addEventListener('scroll', () => {

//   let scrollPosition = window.scrollY;
//   let navEl = document.getElementById('services');
//   let navElPosition = document.getElementById('services').offsetTop;

//   if (scrollPosition > navElPosition - 500) {
//     let activeEl = document.querySelector('.active').parentElement;
//     activeEl.chi

//     console.log(activeEl);
//   }
// })